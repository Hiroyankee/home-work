import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HomeWork {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton ramenButton;
    private JTextPane receivesInfo;
    private JButton checkOutButton;
    private JLabel ItemLabel;
    private JLabel Total;
    private JLabel yenLabel;
    private JLabel priceLabel;

    int price = 0;
    void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            String current = receivesInfo.getText();
            receivesInfo.setText(current+food+"\n");
            price+=100;
            priceLabel.setText(String.valueOf(price));
            JOptionPane.showMessageDialog(null,"Thank you for ordering "+food+"! " +
                    "It will be served as soon as possible");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("HomeWork");
        frame.setContentPane(new HomeWork().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public HomeWork() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to Checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0){
                    JOptionPane.showMessageDialog(null,"Thank you. The total price " +
                            "is "+price +" yen.");
                    price=0;
                    priceLabel.setText(String.valueOf(price));
                    receivesInfo.setText("");
                }
            }
        });
    }
}
